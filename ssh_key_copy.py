'''
Author: Przemysław Kozak
'''


import argparse
import sys
import pexpect
from subprocess import run, PIPE
from getpass import getpass


def push_key(user, hosts):
    '''Push your SSH Key to remote server'''
    for host in hosts:
        command = f'ssh-copy-id -o StrictHostKeyChecking=no {user}@{host}'
        result = run(command, shell=True, stdout=PIPE, stderr=PIPE, universal_newlines=True, check=False)
        if "All keys were skipped because they already exist on the remote system" in result.stderr:
            print(f'{host}: Skipping. SSH Key already exists')
        elif "1 key(s) remain to be installed" in result.stderr:
            print(f'{host}: Installing SSH Key')
        elif "password:" in result.stdout:
            print(f'{host}: AALAL')
        elif "ERROR: ssh: Could not resolve hostname" in result.stderr:
            print(f'{host}: Error. Could not resolve hostname')


def push_key2(user, hosts):
    for host in hosts:
        #pwd = getpass("password: ")
        child = pexpect.spawn(f'ssh-copy-id -o StrictHostKeyChecking=no {user}@{host}', encoding='utf-8')
        #child.sendline('brzoza!@sosNaToR02')
        #child.expect('password:')
        #child.sendline('brzoza!@sosNaToR02')

        #print(child.after,child.before)
        #print(child.after)
        #print('aaa')
        #print(f'To jest {i}')

        i = child.expect(['.*ssword.*:', '.*if you think this is a mistake, you may want to use -f option.*'])
        if i == 0:
            child.sendline('YourPassword')
            child.expect(pexpect.EOF, timeout=None)
            cmd_show_data = child.before
            cmd_output = cmd_show_data.split('\r\n')
            for data in cmd_output:
                print(data)
        else:
            child.expect(pexpect.EOF, timeout=None)
            print('Nothing')



def main():
    '''Start of script'''
    parser = argparse.ArgumentParser(description='Script for sending SSH Key to remote hosts')
    parser.add_argument('-u', '--user', default='root', help='Type SSH username. Default is root')
    parser.add_argument('-s', '--host', help='Type IP or hostname of remote server')
    parser.add_argument('-f', '--file', help='Pass name of file containing list of hosts')
    args = parser.parse_args()

    if len(sys.argv) == 1: #If user not specify any input arguments print help and exit
        parser.print_help()
        parser.exit()

    elif args.file:
        with open(args.file, encoding='utf8') as file:
            hosts = [line.strip() for line in file]
            push_key2(args.user, hosts)

    else:
        hosts = [args.host]
        push_key2(args.user, hosts)


if __name__ == '__main__':
    main()
